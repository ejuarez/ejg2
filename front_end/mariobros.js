// variables, constantes, consola
const personaje = "Mario";

console.log(personaje);
console.info(personaje);
console.warn(personaje);
console.error(personaje);


for (let i = 1; i <= 10; i++) {
    console.log(i);
}

//La siguiente línea mandaría un error, por la variable i murió con el cierre de la llave anterior
//console.log(i);

for (let i = 1; i <= 10; i++) {
    console.log(i);
}


// alert, prompt, confirm
alert("Esto es una alerta");

const nombre = prompt("¿Cómo te llamas?");
console.log(nombre);

const respuesta = confirm("¿Estás seguro?");
console.log(respuesta);


// funciones tradicionales
function saltar () {
    console.log("Mario salta");
}

saltar();


// funciones modernas
const correr = (personaje) => console.log(personaje + " corre");

correr(personaje);


// arreglos
const personajes = ["Luigi"];
personajes.push(personaje);
personajes[2] = "Yoshi";
personajes[10] = "Peach";
personajes.length = 20;

console.log(personajes);

personajes.length = 5;

console.log(personajes);

personajes["nivel"] = 1;

console.log(personajes);

document.getElementById("salida").innerHTML = "Aquí van las salidas para el laboratorio";